package agi

import (
    "strconv"
//    "fmt"
)

// Value结构
type JsonValue struct {
    Value   interface{}
}

type JsonString struct {
    Value   string
}

type JsonNumber struct {
    Value   float64
}

//func (item *JsonValue) TypeErase()

// 尝试将JsonValue的值转换为一个字符串
func (item *JsonValue) AsString() string {
    v := item.Value
    switch v.(type) {
        case nil :
        return ""
        case string :
        if val, ok := v.(string); ok {
            return val
        }
        case bool :
        if v == true {
            return "true"
        } else {
            return "false"
        }
        // 实际上是没有整型值
        // case int :
        case float64 :
        if val, ok := item.Value.(float64); ok {
            return strconv.FormatFloat(val, 'f', -1, 64)
        }
        case []interface{} :
        // 数组暂时就不做特别处理了
        default:
        // 其它未知数据，返回空值
    }
    return ""
}

// 尝试将JsonValue转换为一个数字类型
func (item *JsonValue) AsNumber() float64 {
    v := item.Value
    switch v.(type) {
        case nil :
        return 0
        case string :
        if val, ok := v.(string); ok {
            float, err := strconv.ParseFloat(val, 64)
            if err == nil {
                return float
            }
        }
        case bool :
        if v == true {
            return 1
        } else {
            return 0
        }
        // 实际上是没有整型值
        // case int :
        case float64 :
        if val, ok := item.Value.(float64); ok {
            return val
        }
        case []interface{} :
        // 数组暂时就不做特别处理了
        default :
        // 其它未知数据，返回空值
    }
    return 0
}

// 检查值是否为null
func (item *JsonValue) IsDefined() bool {
    v := item.Value
    switch v.(type) {
        case nil :
            return true
    }
    return false
}

// 是否为空值，nil, 字符串 "", 数字的0, false, 空数组，都为空
//func (item *JsonValue) IsEmpty() bool {
//    v := item.Value
//    switch v.(type) {
//        case nil :
//            return true
//        case string :
//            if val, ok := v.(string); ok {
//                return val
//            }
//    }
//    return false
//}

// 将值类型擦写为字符串类型
func EraseToString(value *interface{}) string {
    v := *value
    switch v.(type) {
        case nil :
        return ""
        case string :
        if erase, ok := v.(string); ok {
            return erase
        }
        case bool :
        if v == true {
            return "true"
        } else {
            return "false"
        }
        // 实际上是没有整型值
        // case int :
        case float64 :
        if erase, ok := v.(float64); ok {
            return strconv.FormatFloat(erase, 'f', -1, 64)
        }
        case []interface{} :
        // 数组暂时就不做特别处理了
        default:
        // 其它未知数据，返回空值
    }
    return ""
}

// 将值类型擦写为字符串类型
func EraseToNumber(value *interface{}) float64 {
    v := *value
    switch v.(type) {
        case nil :
        return 0
        case string :
        if val, ok := v.(string); ok {
            float, err := strconv.ParseFloat(val, 64)
            if err == nil {
                return float
            }
        }
        case bool :
        if v == true {
            return 1
        } else {
            return 0
        }
        // 实际上是没有整型值
        // case int :
        case float64 :
        if val, ok := v.(float64); ok {
            return val
        }
        case []interface{} :
        // 数组暂时就不做特别处理了
        default:
        // 其它未知数据，返回空值
    }
    return 0
}
