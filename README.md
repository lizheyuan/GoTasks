#GoTasks

呃，不用说明了吧，直接go build就可以随意玩了。

已通过编译的版本包括：`go1.2.1 linux/amd64`、`go1.4.2 windows/amd64`。

目前我已经持续运行了超过12个小时，15个任务，其中有2个是秒发的任务，非常稳定没有中断过。

Http任务的执行是go并发的，绝不延迟，同一个任务，下一次执行不会受到上一次任务的延迟而迟滞，所以请确定接收服务器能承受住压力才好使用。

日志库用了这个[https://github.com/donnie4w/go-logger](https://github.com/donnie4w/go-logger)，不过我修改了一些。

Golang应该是并发编程领域，开发效率最高，且可精确调度CPU时钟，从他的时间库的设计，我相信完全可以精确到毫秒、微秒。

```
GoTasks.exe --help
Usage of GoTasks.exe:
  -cycle=43200: 重新加载任务的周期，单位秒
  -log-dir=".": 日志输出目录
  -log-level=1: 日志输出级别
  -tasks="": 任务列表
  -trace-cycle=600: 输出心跳信息的周期，单位秒
  -show-complete=1: 显示请求完成的信息
```

执行

```
GoTasks -tasks="http://localhost/tasks.json" -cycle=43200
```

给一个tasks.json的清单样例：

```json
{
	"Tasks":[
		{
			"Name":"kissOschina",
			"Url":"http://www.oschina.net/",
			"Cycle":1,
			"PostUrl":""
		},
		{
			"Name":"kissBaidu",
			"Url":"http://www.baidu.com/",
			"Cycle":1,
			"PostUrl":""
		}
	]
}
```

* Name 任务名称
* Url 要取得数据的Url
* Cycle 获取的周期，单位秒
* PostUrl 从Url取得的页面的内容，将他Post到另一个Url上

